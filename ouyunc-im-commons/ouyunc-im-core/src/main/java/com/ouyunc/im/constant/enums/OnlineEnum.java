package com.ouyunc.im.constant.enums;

/**
 * @Author fangzhenxun
 * @Description: 在线状态
 **/
public enum OnlineEnum {

    /**
     * 离线
     */
    OFFLINE,

    /**
     * 在线
     */
    ONLINE,
}
