package com.ouyunc.im.cache.l1;

import com.ouyunc.im.cache.ICache;

/**
 * @Author fangzhenxun
 * @Description: 一级缓存
 **/
public interface L1Cache<K, V> extends ICache<K, V> {

}
