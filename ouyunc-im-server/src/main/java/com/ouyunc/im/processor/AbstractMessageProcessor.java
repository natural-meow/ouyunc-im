package com.ouyunc.im.processor;

import com.alibaba.fastjson2.JSON;
import com.ouyunc.im.constant.enums.MessageTypeEnum;
import com.ouyunc.im.helper.DbHelper;
import com.ouyunc.im.packet.Packet;
import com.ouyunc.im.packet.message.ExtraMessage;
import com.ouyunc.im.packet.message.InnerExtraData;
import com.ouyunc.im.packet.message.Message;
import com.ouyunc.im.validate.MessageValidate;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.BiConsumer;

/**
 * @Author fangzhenxun
 * @Description: Im消息抽象处理类
 **/
public abstract class AbstractMessageProcessor implements MessageProcessor {
    private static Logger log = LoggerFactory.getLogger(AbstractMessageProcessor.class);

    /**
     * 标识子类处理消息的类型，如果一个子类处理多个类型使用 | 逻辑或进行返回
     */
    public abstract MessageTypeEnum messageType();

    /**
     * @param ctx
     * @param packet
     * @return void
     * @Author fangzhenxun
     * @Description 默认做鉴权处理，业务处理器可以不用重写该方法
     */
    @Override
    public void preProcess(ChannelHandlerContext ctx, Packet packet) {
        log.info("现在处理默认的前置处理 packet: {} ...", packet);
        // 存储packet到数据库中（目前只是保存相关信息，不做扩展，以后可以做数据分析使用）
        Message message = (Message) packet.getMessage();
        ExtraMessage extraMessage = JSON.parseObject(message.getExtra(), ExtraMessage.class);
        InnerExtraData innerExtraData = extraMessage.getInnerExtraData();
        String appKey = innerExtraData.getAppKey();
        EVENT_EXECUTORS.execute(() -> DbHelper.writeMessage(appKey, packet));
        if (!MessageValidate.isAuth(appKey, message.getFrom(), packet.getDeviceType(), ctx)) {
            return;
        }
        // 交给下个处理
        ctx.fireChannelRead(packet);
    }


    /**
     * 传递处理器
     *
     * @param ctx
     * @param packet
     * @param function
     */
    protected void fireProcess(ChannelHandlerContext ctx, Packet packet, BiConsumer<ChannelHandlerContext, Packet> function) {
        log.info("正在使用默认处理器来处理消息");
        function.accept(ctx, packet);
        // 交给下个处理器去处理
        ctx.fireChannelRead(packet);
    }

    /**
     * @param ctx
     * @param packet
     * @return void
     * @Author fangzhenxun
     * @Description 做默认后置处理
     */
    @Override
    public void postProcess(ChannelHandlerContext ctx, Packet packet) {
        log.info("现在处理默认的后置处理 packet: {} ...", packet);
        // 默认往下面传递，交给qos后置处理逻辑
        ctx.fireChannelRead(packet);
    }
}
